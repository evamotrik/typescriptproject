"use strict";
exports.__esModule = true;
var jwt = require("jsonwebtoken");
var config_1 = require("../helpers/config");
var authMiddleware = function (req, res, next) {
    if (req.method === "OPTIONS") {
        next();
    }
    try {
        var token = req.headers.authorization.split(" ")[1];
        if (!token) {
            return res.status(403).json({ message: "Пользователь не авторизован" });
        }
        var decodedData = jwt.verify(token, config_1.secret);
        req.user = decodedData;
        next();
    }
    catch (e) {
        return res.status(403).json({ message: "Пользователь не авторизован" });
    }
};
exports["default"] = authMiddleware;
