"use strict";
exports.__esModule = true;
var jwt = require("jsonwebtoken");
var config_1 = require("../helpers/config");
var roleMiddleware = function (role) { return function (req, res, next) {
    if (req.method === "OPTIONS") {
        next();
    }
    try {
        var token = req.headers.authorization.split(" ")[1];
        if (!token) {
            return res.status(403).json({ message: "Пользователь не авторизован" });
        }
        var userRole = jwt.verify(token, config_1.secret).role;
        var hasRole = false;
        if (userRole.toString().includes(role.toString())) {
            hasRole = true;
        }
        if (!hasRole) {
            return res.status(403).json({ message: "У вас нет доступа" });
        }
        next();
    }
    catch (e) {
        return res.status(403).json({ message: "У вас нет доступа" });
    }
}; };
exports["default"] = roleMiddleware;
