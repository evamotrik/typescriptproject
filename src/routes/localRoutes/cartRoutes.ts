import  * as express from 'express'
import cartController from '../../controllers/cartController' 
import authMiddleware from '../../middleware/authMiddleware'
const router = express.Router()

router.post("/create",authMiddleware, cartController.cartCreate);
router.post("/addProducts",authMiddleware, cartController.addProducts);    
router.get("/getbyId/:id", cartController.getById);

export default router;