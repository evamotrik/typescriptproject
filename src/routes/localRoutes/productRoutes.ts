import * as express from 'express'
import productController from '../../controllers/productController' 
import roleMiddleware from '../../middleware/roleMiddleware'
const router = express.Router()

router.post("/create", roleMiddleware('ADMIN'), productController.productCreate);    
router.delete("/:id", roleMiddleware('ADMIN'), productController.deleteProduct);
router.post("/update/:id", roleMiddleware('ADMIN'), productController.updateProduct);
router.get("/getbyId/:id", productController.getById);
router.get("/getAll", productController.getAll);

export default router;