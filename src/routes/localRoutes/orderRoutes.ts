import  * as express from 'express'
import orderController from '../../controllers/orderController' 
import roleMiddleware from '../../middleware/roleMiddleware'
import authMiddleware from '../../middleware/authMiddleware'
const router = express.Router()

router.post("/create", authMiddleware, orderController.orderCreate); 
router.delete("/:id", authMiddleware, orderController.deleteOrder);
router.get("/getbyId/:id",authMiddleware, orderController.getById);
router.get("/getAll", authMiddleware, orderController.getAll);

export default router;