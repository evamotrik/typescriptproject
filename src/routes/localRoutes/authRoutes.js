"use strict";
exports.__esModule = true;
var express = require("express");
var authController_1 = require("../../controllers/authController");
var router = express.Router();
router.post('/registration', authController_1["default"].registration);
router.post('/login', authController_1["default"].login);
// router.get('/google', authController.createGoogleUrl)
// router.get('/google/redirect', authController.getToken)
exports["default"] = router;
