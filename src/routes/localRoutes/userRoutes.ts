import  * as express from 'express'

import authMiddleware from '../../middleware/authMiddleware'
import roleMiddleware from '../../middleware/roleMiddleware'

import userController from '../../controllers/userController' 
const router = express.Router()

router.post("/create", roleMiddleware('ADMIN'),userController.userCreate);    
router.delete("/:id", roleMiddleware('ADMIN'), userController.deleteUser);
router.get("/getbyid/:id", authMiddleware, userController.getById);
router.get("/getall", authMiddleware, userController.getAll);
router.put("/:id", authMiddleware, userController.updateUser);


export default router;
