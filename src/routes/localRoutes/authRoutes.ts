import * as express from 'express'
import authController from '../../controllers/authController'
const router = express.Router()

router.post('/registration', authController.registration)
router.post('/login', authController.login)
// router.get('/google', authController.createGoogleUrl)
// router.get('/google/redirect', authController.getToken)

export default router

