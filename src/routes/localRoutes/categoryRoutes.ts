import  * as express from 'express'

import categoryController from '../../controllers/categoryController'  
import roleMiddleware from '../../middleware/roleMiddleware'
import authMiddleware from '../../middleware/authMiddleware'
const router = express.Router()

router.post("/create", roleMiddleware('ADMIN'), categoryController.createCategory);    
router.delete("/:id", roleMiddleware('ADMIN'), categoryController.deleteCategory);
router.post("/update/:id", roleMiddleware('ADMIN'), categoryController.updateCategory);
router.get("/getbyId/:id", authMiddleware,  categoryController.getById);
router.get("/getAll", authMiddleware, categoryController.getAll);

export default router;