import * as express from 'express'
import authRoutes from "./localRoutes/authRoutes"
import userRoutes from './localRoutes/userRoutes'
import categoryRoutes from './localRoutes/categoryRoutes'
import productRoutes from './localRoutes/productRoutes'
import cartRoutes from './localRoutes/cartRoutes'
import orderRoutes from './localRoutes/orderRoutes'

const router = express.Router()

router.use('/auth', authRoutes);
router.use('/user', userRoutes);
router.use('/category', categoryRoutes);
router.use('/product', productRoutes);
router.use('/cart', cartRoutes);
router.use('/order', orderRoutes)



export default router


