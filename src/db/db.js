"use strict";
exports.__esModule = true;
var mongoose = require("mongoose");
var config_1 = require("../helpers/config");
var db = mongoose.connect(config_1.mongoUri, 
// { useFindAndModify: false},
function (err) {
    if (!err) {
        console.log("MongoDB Connection Succeeded");
    }
    else {
        console.log("Error in DB connection: " + err);
    }
});
exports["default"] = db;
