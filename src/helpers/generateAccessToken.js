"use strict";
exports.__esModule = true;
var jwt = require("jsonwebtoken");
var config_1 = require("./config");
var generateAccessToken = function (id, role) {
    var payload = {
        id: id,
        role: role
    };
    return jwt.sign(payload, config_1.secret, { expiresIn: "24h" });
};
exports["default"] = generateAccessToken;
