import * as jwt from 'jsonwebtoken'
import {secret} from './config'

const generateAccessToken = (id: any, role:any) =>{
    const payload = {
        id,
        role
    }
    return jwt.sign(payload, secret, {expiresIn: "24h"})
}

export default generateAccessToken