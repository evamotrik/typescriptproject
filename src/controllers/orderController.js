"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
exports.__esModule = true;
var Order_1 = require("../models/Order");
var User_1 = require("../models/User");
var Cart_1 = require("../models/Cart");
var OrderController = /** @class */ (function () {
    function OrderController() {
    }
    OrderController.prototype.orderCreate = function (req, res) {
        return __awaiter(this, void 0, void 0, function () {
            var order, user, cart, e_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 7, , 8]);
                        order = new Order_1.Order();
                        return [4 /*yield*/, User_1.User.findOne({ _id: req.user.id })];
                    case 1:
                        user = _a.sent();
                        if (!user) return [3 /*break*/, 4];
                        return [4 /*yield*/, Cart_1.Cart.findOne({ _id: user.cart })];
                    case 2:
                        cart = _a.sent();
                        order.user = req.user.id;
                        if (!cart) return [3 /*break*/, 4];
                        order.cart = cart.products;
                        return [4 /*yield*/, order.save()];
                    case 3:
                        _a.sent();
                        _a.label = 4;
                    case 4:
                        if (!user) return [3 /*break*/, 6];
                        Cart_1.Cart.deleteOne({ _id: user.cart }, function () {
                            return res.send("Cart delete error");
                        });
                        user.cart = null;
                        user.order.push(order._id);
                        return [4 /*yield*/, user.save()];
                    case 5:
                        _a.sent();
                        res.json({ order: order });
                        _a.label = 6;
                    case 6: return [3 /*break*/, 8];
                    case 7:
                        e_1 = _a.sent();
                        res.status(400).json({ message: "Ошибка созания заказа" });
                        return [3 /*break*/, 8];
                    case 8: return [2 /*return*/];
                }
            });
        });
    };
    OrderController.prototype.deleteOrder = function (req, res) {
        return __awaiter(this, void 0, void 0, function () {
            var order, e_2;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, Order_1.Order.findOne({ _id: req.params.id })];
                    case 1:
                        order = _a.sent();
                        if (order) {
                            if (req.user.role === 'ADMIN' || req.user.id.toString() === order.user.toString()) {
                                deleteFromUser(order._id, order.user);
                                Order_1.Order.deleteOne({ _id: req.params.id }, function () {
                                    res.send("\u0417\u0430\u043A\u0430\u0437 \u0443\u0434\u0430\u043B\u0435\u043D");
                                });
                            }
                            else {
                                res.json({ message: "Заказ не принадлежит Вам" });
                            }
                        }
                        else
                            res.send("\u0422\u0430\u043A\u043E\u0433\u043E \u0437\u0430\u043A\u0430\u0437\u0430 \u043D\u0435 \u0441\u0443\u0449\u0435\u0441\u0442\u0432\u0443\u0435\u0442");
                        return [3 /*break*/, 3];
                    case 2:
                        e_2 = _a.sent();
                        res.status(400).json({ message: "Deleting order error -> " + e_2 });
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    OrderController.prototype.getById = function (req, res) {
        return __awaiter(this, void 0, void 0, function () {
            var order, e_3;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, Order_1.Order.findOne({ _id: req.params.id })];
                    case 1:
                        order = _a.sent();
                        if (order) {
                            res.json({ order: order });
                        }
                        else
                            res.send("\u0422\u0430\u043A\u043E\u0433\u043E \u0437\u0430\u043A\u0430\u0437\u0430 \u043D\u0435 \u0441\u0443\u0449\u0435\u0441\u0442\u0432\u0443\u0435\u0442");
                        return [3 /*break*/, 3];
                    case 2:
                        e_3 = _a.sent();
                        res.status(400).json({ message: "Get Orders ID error -> " + e_3 });
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    OrderController.prototype.getAll = function (req, res) {
        return __awaiter(this, void 0, void 0, function () {
            var order, e_4;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 5, , 6]);
                        order = void 0;
                        if (!(req.user.role.toString() === 'ADMIN')) return [3 /*break*/, 2];
                        return [4 /*yield*/, Order_1.Order.find()];
                    case 1:
                        order = _a.sent();
                        res.json({ message: order });
                        return [3 /*break*/, 4];
                    case 2: return [4 /*yield*/, Order_1.Order.find({ user: req.user.id })];
                    case 3:
                        order = _a.sent();
                        res.json({ message: order });
                        _a.label = 4;
                    case 4: return [3 /*break*/, 6];
                    case 5:
                        e_4 = _a.sent();
                        res.status(400).json({ message: "error -> " + e_4 });
                        return [3 /*break*/, 6];
                    case 6: return [2 /*return*/];
                }
            });
        });
    };
    return OrderController;
}());
var deleteFromUser = function (orderId, userId) { return __awaiter(void 0, void 0, void 0, function () {
    var user, i;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0: return [4 /*yield*/, User_1.User.findOne({ _id: userId })];
            case 1:
                user = _a.sent();
                if (!user) return [3 /*break*/, 5];
                i = 0;
                _a.label = 2;
            case 2:
                if (!(i < user.order.length)) return [3 /*break*/, 5];
                if (!(user.order[i].toString() === orderId.toString())) return [3 /*break*/, 4];
                user.order.splice(i, 1);
                return [4 /*yield*/, user.save()];
            case 3:
                _a.sent();
                return [3 /*break*/, 5];
            case 4:
                i++;
                return [3 /*break*/, 2];
            case 5: return [2 /*return*/];
        }
    });
}); };
exports["default"] = new OrderController;
