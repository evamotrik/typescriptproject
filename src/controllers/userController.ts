import {User, Iuser} from "../models/User"
import { Request, Response } from 'express'
import * as bCrypt from "bcrypt"

interface userRequest<T> extends Request {
    body: T
}

class UserController{
    async userCreate(req: userRequest <Iuser>, res: Response){
        try{
            const {email, password, name, surname, role} = req.body
            const candidate = await User.findOne({email})
            if(candidate){
                return res.status(400).json({message:"Пользователь с такой почтой уже существует"})
            }
            const hashPassword = bCrypt.hashSync(password, 7)
            User.create({email, password: hashPassword, name, surname, role}, function(err, doc){    
            if(err) return console.log(err);
            res.send("User saved" + doc);
        });
    }catch (e){
        res.status(400).json({message: "Adding User error -> " + e})
    }
    }
    
    async deleteUser (req: userRequest <Iuser>, res: Response) {
        try{
            const candidate = await User.findOne({_id: req.params.id})
            if(candidate){
                User.deleteOne({_id: req.params.id}, function() {
                    res.send(`Пользователь удален`);
                });
            }
            else res.json({message:`Такого пользователя не существует`});
        }catch (e){
            res.status(400).json({message: "Deleting User error -> " + e})
        }
    }

    async getById (req: userRequest <Iuser>, res: Response){
        try{
            const candidate = await User.findOne({_id: req.params.id})
            if(candidate){
                User.findOne({_id: req.params.id}, function(err: String, doc: any) {
                    if (err) return res.send("error");
                    res.send( doc);
                });
            }
            else res.json({message:`Такого пользователя не существует`});
             
        }catch (e){
            res.status(400).json({message: "Get User ID error -> " + e})
        } 
    }

    async getAll (req: userRequest <Iuser>, res: Response){
        try{
            let users = await User.find()
            res.json({users: users})
        }catch (e){
            res.status(400).json({message: "Ошибка вывода -> " + e})
        } 
    }

    async updateUser (req: userRequest <Iuser>, res: Response){
        try{
            const {email, password, name, surname} = req.body
            const hashPassword = bCrypt.hashSync(password, 7)
            if(req.user.role === 'ADMIN' || req.user.id === req.params.id){
                await User.updateOne({_id: req.params.id}, {email, password: hashPassword, name, surname})
            }else{
                res.json({message:"Вы не можете изменить другого пользователя"})
            }
            res.json({message:"Пользователь изменен"})
        }catch (e){
            res.status(400).json({message: "Update User error -> " + e})
        } 
    }

}

export default new UserController;

