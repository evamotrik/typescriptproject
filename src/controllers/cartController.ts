import {User, Iuser} from "../models/User"
import {Cart, Icart} from "../models/Cart"
import { Request, Response } from 'express'

interface cartRequest<T> extends Request {
    body: T
}

class CartController{
    async cartCreate(req: cartRequest <Icart>, res: Response){
        try{
            let cart = new Cart();
            let user = await User.findOne({_id : req.user.id});

            cart.user = req.user.id;
            await cart.save(); 

            if(user){
                user.cart = cart._id;
                await user.save();
                res.json({cart});
            }

        }catch (e){
            res.status(400).json({message: "Ошибка создания корзины"})
        }
    }

    async addProducts(req: cartRequest <Icart>, res: Response){
        try{ 
            let user = await User.findOne({_id: req.user.id});
            if(user){
                 let cart = await Cart.findOne({_id: user.cart});
                let product = req.body.id;
                if(cart){
                    cart.products.push(product);
                    await cart.save();
                    res.json({message: `Продукт добавлен в корзину`});
                } 
            }
        }catch (e){
            res.status(400).json({message: "Ошибка добавления в корзину"})
        }
    }

    async getById(req: cartRequest <Icart>, res: Response){
        try{
            const cart = await Cart.findOne({_id: req.params.id})
            if(cart){
                Cart.findOne({_id: req.params.id}, function(err: any, doc: any) {
                    if (err) return res.send("error");
                    res.send(`Информация о корзине ${doc}`);
                });
            }
            else res.send(`Такой корзины не существует`);
             
        }catch (e){
            res.status(400).json({message: "Get Carts ID error -> " + e})
        } 
    }

}

export default new CartController;

