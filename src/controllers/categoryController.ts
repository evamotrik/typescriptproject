import * as e from "express"
import { Request, Response } from 'express'
import {Category, Icategory} from "../models/Category"
import {Product, Iproduct} from "../models/Product"

interface categoryRequest<T> extends Request {
    body: T
}

class CategoryController{

    async createCategory(req: categoryRequest <Icategory>, res: Response){
        try{
            const name = req.body.name
            const category = await Category.findOne({name})
            if(category){
                return res.status(400).json({message:"Такая категория уже существует"})
            }
            const obj = await Category.create({name})
            res.json({message: obj})
        }catch (e){
            res.status(400).json({message: "Creating category error -> " + e})
        }
    }

    async deleteCategory(req: categoryRequest <Icategory>, res: Response){
        try{
            const category = await Category.findOne({_id: req.params.id})
            if(category){
                deleteFromAllProducts(req.params.id)
                Category.deleteOne({_id: req.params.id}, (err) =>{
                    if (err) return res.send("Ошибка удаления");
                    res.send(`Категория удалена`);
                })
            }
        }catch (e){
            res.status(400).json({message: "Ошибка удаления -> " + e})
        }
    }

    async updateCategory(req: any, res: any){
        try{
            const category = await Category.findOne({_id: req.params.id})
            if(!category){
                return res.status(400).json({message:"Нет такой категории"})
            }
            category.name = req.body.name
            category.products.push(req.body.product)
            category.save()
            const product = await Product.findOne({_id: req.body.product})
            if(product){
            product.category = req.params.id
            product.save()
            res.send("Категония изменена")
            } 
        }catch (e){
            res.status(400).json({message: "Update User error -> " + e})
        } 
    }
    
    async getById(req: categoryRequest <Icategory>, res: Response){
        try{
            const category = await Category.findOne({_id: req.params.id})
            res.json({category: category})
        }catch (e){
            res.status(400).json({message: "Get category ID error -> " + e})
        } 
    }
    
    async getAll(req: categoryRequest <Icategory>, res: Response){
        try{
            let categories = await Category.find()
            res.json({categories: categories})
        }catch (e){
            res.status(400).json({message: "Ошибка вывода -> " + e})
        } 
    }
}  

const deleteFromAllProducts = async (categoryId: any)=>{
    const products = await Product.find({category: categoryId})
    for(let i=0;i<products.length;i++){
        products[i].category = null
        products[i].save()
    }
} 



export default new CategoryController;
