import {Category, Icategory} from "../models/Category"
import {Product, Iproduct} from "../models/Product"
import { Request, Response } from 'express'

interface productRequest<T> extends Request {
    body: T
}

class ProductController{
    async productCreate(req: productRequest <Iproduct>, res: Response){
        try{
            const {name} = req.body
            const product = await Product.findOne({name})
            if(product){
                return res.status(400).json({message:"Такой продукт уже существует"})
            }
            const obj = await Product.create(req.body)
            addProductToCategory(obj._id, obj.category)
            res.json({message: obj})
        }catch (e){
            res.status(400).json({message: "Adding product error -> " + e})
        }
    }

    async deleteProduct (req: productRequest <Iproduct>, res: Response) {
        try{
            const product = await Product.findOne({_id: req.params.id})
            if(product){
                deleteFromCategory(product.category, req.params.id)
                Product.deleteOne({_id: req.params.id}, function() {
                    res.send(`Продукт удален`);
                });
            }
            else res.send(`Такого продукта не существует`);
             
        }catch (e){
            res.status(400).json({message: "Deleting product error -> " + e})
        }
    }

    async getById (req: productRequest <Iproduct>, res: Response){
        try{
            const product = await Product.findOne({_id: req.params.id})
            res.json({product})
        }catch (e){
            res.status(400).json({message: "Get Product ID error -> " + e})
        } 
    } 

    async getAll (req: productRequest <Iproduct>, res: Response){
        try{
            const products = await Product.find();
            res.json(products);   
        }catch (e){
            res.status(400).json({message: "Get Products list error -> " + e})
        }
    }

    async updateProduct (req: productRequest <Iproduct>, res: Response){
        try{
            const product = await Product.findOne({_id: req.params.id});
            if(!product){
                return res.status(400).json({message:"Нет такого продукта"})
            }
            const {name, price, category} = req.body;
            console.log(category);
            res.send({ message: category })
            if(category || category===null){
                deleteFromCategory(product.category, product._id)
            }
            await Product.updateOne({_id: req.params.id}, {name, price, category}) 
            addProductToCategory(product._id, category)
            res.json({message:"Продукт изменен"})
        }catch (e){
            res.status(400).json({message: "Update product error -> " + e})
        } 
    }
    
}

const addProductToCategory = async(productId: string, categoryId: string)=>{
    const category = await Category.findOne({_id: categoryId})
    if(category){
        category.products.push(productId)
        category.save()
    } 
}
    
const deleteFromCategory = async (categoryId: string, productId: string)=>{
    let category = await Category.findOne({_id: categoryId})
    if(category){
        for(let i = 0; i < category.products.length; i++){
            if(category.products[i].toString() === productId.toString()){
                category.products.splice(i, 1)
                await category.save()
                break
            }
        }
    }
} 
export default new ProductController;

