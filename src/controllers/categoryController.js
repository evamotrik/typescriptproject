"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
exports.__esModule = true;
var Category_1 = require("../models/Category");
var Product_1 = require("../models/Product");
var CategoryController = /** @class */ (function () {
    function CategoryController() {
    }
    CategoryController.prototype.createCategory = function (req, res) {
        return __awaiter(this, void 0, void 0, function () {
            var name_1, category, obj, e_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 3, , 4]);
                        name_1 = req.body.name;
                        return [4 /*yield*/, Category_1.Category.findOne({ name: name_1 })];
                    case 1:
                        category = _a.sent();
                        if (category) {
                            return [2 /*return*/, res.status(400).json({ message: "Такая категория уже существует" })];
                        }
                        return [4 /*yield*/, Category_1.Category.create({ name: name_1 })];
                    case 2:
                        obj = _a.sent();
                        res.json({ message: obj });
                        return [3 /*break*/, 4];
                    case 3:
                        e_1 = _a.sent();
                        res.status(400).json({ message: "Creating category error -> " + e_1 });
                        return [3 /*break*/, 4];
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    CategoryController.prototype.deleteCategory = function (req, res) {
        return __awaiter(this, void 0, void 0, function () {
            var category, e_2;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, Category_1.Category.findOne({ _id: req.params.id })];
                    case 1:
                        category = _a.sent();
                        if (category) {
                            deleteFromAllProducts(req.params.id);
                            Category_1.Category.deleteOne({ _id: req.params.id }, function (err) {
                                if (err)
                                    return res.send("Ошибка удаления");
                                res.send("\u041A\u0430\u0442\u0435\u0433\u043E\u0440\u0438\u044F \u0443\u0434\u0430\u043B\u0435\u043D\u0430");
                            });
                        }
                        return [3 /*break*/, 3];
                    case 2:
                        e_2 = _a.sent();
                        res.status(400).json({ message: "Ошибка удаления -> " + e_2 });
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    CategoryController.prototype.updateCategory = function (req, res) {
        return __awaiter(this, void 0, void 0, function () {
            var category, product, e_3;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 3, , 4]);
                        return [4 /*yield*/, Category_1.Category.findOne({ _id: req.params.id })];
                    case 1:
                        category = _a.sent();
                        if (!category) {
                            return [2 /*return*/, res.status(400).json({ message: "Нет такой категории" })];
                        }
                        category.name = req.body.name;
                        category.products.push(req.body.product);
                        category.save();
                        return [4 /*yield*/, Product_1.Product.findOne({ _id: req.body.product })];
                    case 2:
                        product = _a.sent();
                        if (product) {
                            product.category = req.params.id;
                            product.save();
                            res.send("Категония изменена");
                        }
                        return [3 /*break*/, 4];
                    case 3:
                        e_3 = _a.sent();
                        res.status(400).json({ message: "Update User error -> " + e_3 });
                        return [3 /*break*/, 4];
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    CategoryController.prototype.getById = function (req, res) {
        return __awaiter(this, void 0, void 0, function () {
            var category, e_4;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, Category_1.Category.findOne({ _id: req.params.id })];
                    case 1:
                        category = _a.sent();
                        res.json({ category: category });
                        return [3 /*break*/, 3];
                    case 2:
                        e_4 = _a.sent();
                        res.status(400).json({ message: "Get category ID error -> " + e_4 });
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    CategoryController.prototype.getAll = function (req, res) {
        return __awaiter(this, void 0, void 0, function () {
            var categories, e_5;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, Category_1.Category.find()];
                    case 1:
                        categories = _a.sent();
                        res.json({ categories: categories });
                        return [3 /*break*/, 3];
                    case 2:
                        e_5 = _a.sent();
                        res.status(400).json({ message: "Ошибка вывода -> " + e_5 });
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    return CategoryController;
}());
var deleteFromAllProducts = function (categoryId) { return __awaiter(void 0, void 0, void 0, function () {
    var products, i;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0: return [4 /*yield*/, Product_1.Product.find({ category: categoryId })];
            case 1:
                products = _a.sent();
                for (i = 0; i < products.length; i++) {
                    products[i].category = null;
                    products[i].save();
                }
                return [2 /*return*/];
        }
    });
}); };
exports["default"] = new CategoryController;
