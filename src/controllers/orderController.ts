import {Order, Iorder} from "../models/Order"
import {User,Iuser} from "../models/User"
import {Cart, Icart} from "../models/Cart"
import * as e from "express"
import { Request, Response } from 'express'


interface orderRequest<T> extends Request {
    body: T
}

class OrderController{
    async orderCreate(req: orderRequest <Iorder>, res: Response){
        try{
        let order = new Order();
        let user = await User.findOne({_id: req.user.id});
        if(user){
            let cart = await Cart.findOne({_id: user.cart})
            order.user = req.user.id;
            if(cart){
                order.cart = cart.products;
                await order.save(); 
            }
           
        }
       if(user){
        Cart.deleteOne({_id: user.cart}, ()=>{
            return res.send("Cart delete error");
         });
         user.cart = null
         user.order.push(order._id);
         await user.save();
 
         res.json({order});
       }
       
        }catch (e){
            res.status(400).json({message: "Ошибка созания заказа"})
        }
    }

    async deleteOrder (req: orderRequest <Iorder>, res: Response) {
        try{
            const order = await Order.findOne({_id: req.params.id})
            if(order){
                if(req.user.role === 'ADMIN' || req.user.id.toString() === order.user.toString())
                {
                    deleteFromUser(order._id, order.user)
                    Order.deleteOne({_id: req.params.id}, function() {
                        res.send(`Заказ удален`);
                    });
                }else{
                    res.json({message: "Заказ не принадлежит Вам"})
                }
            }
            else res.send(`Такого заказа не существует`);
        }catch (e){
            res.status(400).json({message: "Deleting order error -> " + e})
        }
    }

    async getById(req: orderRequest <Iorder>, res: Response){
        try{
            const order = await Order.findOne({_id: req.params.id})
            if(order){
                res.json({order})
            }
            else res.send(`Такого заказа не существует`);
             
        }catch (e){
            res.status(400).json({message: "Get Orders ID error -> " + e})
        } 
    }

    async getAll (req: orderRequest <Iorder>, res: Response){
        try{
            let order
            if(req.user.role.toString() === 'ADMIN'){
                order = await Order.find()
                res.json({message: order})
            }else{
                order = await Order.find({user: req.user.id})
                res.json({message: order})
            }
        }catch (e){
            res.status(400).json({message: "error -> " + e})
        } 
    }
}

const deleteFromUser = async (orderId: string, userId: string) =>{
    const user = await User.findOne({_id: userId})
    if(user){
        for(let i = 0; i < user.order.length; i++){
            if(user.order[i].toString() === orderId.toString()){
                user.order.splice(i, 1)
                await user.save()
                break
            }
        }
    }   
}


export default new OrderController;
