"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
exports.__esModule = true;
var Category_1 = require("../models/Category");
var Product_1 = require("../models/Product");
var ProductController = /** @class */ (function () {
    function ProductController() {
    }
    ProductController.prototype.productCreate = function (req, res) {
        return __awaiter(this, void 0, void 0, function () {
            var name_1, product, obj, e_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 3, , 4]);
                        name_1 = req.body.name;
                        return [4 /*yield*/, Product_1.Product.findOne({ name: name_1 })];
                    case 1:
                        product = _a.sent();
                        if (product) {
                            return [2 /*return*/, res.status(400).json({ message: "Такой продукт уже существует" })];
                        }
                        return [4 /*yield*/, Product_1.Product.create(req.body)];
                    case 2:
                        obj = _a.sent();
                        addProductToCategory(obj._id, obj.category);
                        res.json({ message: obj });
                        return [3 /*break*/, 4];
                    case 3:
                        e_1 = _a.sent();
                        res.status(400).json({ message: "Adding product error -> " + e_1 });
                        return [3 /*break*/, 4];
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    ProductController.prototype.deleteProduct = function (req, res) {
        return __awaiter(this, void 0, void 0, function () {
            var product, e_2;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, Product_1.Product.findOne({ _id: req.params.id })];
                    case 1:
                        product = _a.sent();
                        if (product) {
                            deleteFromCategory(product.category, req.params.id);
                            Product_1.Product.deleteOne({ _id: req.params.id }, function () {
                                res.send("\u041F\u0440\u043E\u0434\u0443\u043A\u0442 \u0443\u0434\u0430\u043B\u0435\u043D");
                            });
                        }
                        else
                            res.send("\u0422\u0430\u043A\u043E\u0433\u043E \u043F\u0440\u043E\u0434\u0443\u043A\u0442\u0430 \u043D\u0435 \u0441\u0443\u0449\u0435\u0441\u0442\u0432\u0443\u0435\u0442");
                        return [3 /*break*/, 3];
                    case 2:
                        e_2 = _a.sent();
                        res.status(400).json({ message: "Deleting product error -> " + e_2 });
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    ProductController.prototype.getById = function (req, res) {
        return __awaiter(this, void 0, void 0, function () {
            var product, e_3;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, Product_1.Product.findOne({ _id: req.params.id })];
                    case 1:
                        product = _a.sent();
                        res.json({ product: product });
                        return [3 /*break*/, 3];
                    case 2:
                        e_3 = _a.sent();
                        res.status(400).json({ message: "Get Product ID error -> " + e_3 });
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    ProductController.prototype.getAll = function (req, res) {
        return __awaiter(this, void 0, void 0, function () {
            var products, e_4;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, Product_1.Product.find()];
                    case 1:
                        products = _a.sent();
                        res.json(products);
                        return [3 /*break*/, 3];
                    case 2:
                        e_4 = _a.sent();
                        res.status(400).json({ message: "Get Products list error -> " + e_4 });
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    ProductController.prototype.updateProduct = function (req, res) {
        return __awaiter(this, void 0, void 0, function () {
            var product, _a, name_2, price, category, e_5;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        _b.trys.push([0, 3, , 4]);
                        return [4 /*yield*/, Product_1.Product.findOne({ _id: req.params.id })];
                    case 1:
                        product = _b.sent();
                        if (!product) {
                            return [2 /*return*/, res.status(400).json({ message: "Нет такого продукта" })];
                        }
                        _a = req.body, name_2 = _a.name, price = _a.price, category = _a.category;
                        console.log(category);
                        res.send({ message: category });
                        if (category || category === null) {
                            deleteFromCategory(product.category, product._id);
                        }
                        return [4 /*yield*/, Product_1.Product.updateOne({ _id: req.params.id }, { name: name_2, price: price, category: category })];
                    case 2:
                        _b.sent();
                        addProductToCategory(product._id, category);
                        res.json({ message: "Продукт изменен" });
                        return [3 /*break*/, 4];
                    case 3:
                        e_5 = _b.sent();
                        res.status(400).json({ message: "Update product error -> " + e_5 });
                        return [3 /*break*/, 4];
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    return ProductController;
}());
var addProductToCategory = function (productId, categoryId) { return __awaiter(void 0, void 0, void 0, function () {
    var category;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0: return [4 /*yield*/, Category_1.Category.findOne({ _id: categoryId })];
            case 1:
                category = _a.sent();
                if (category) {
                    category.products.push(productId);
                    category.save();
                }
                return [2 /*return*/];
        }
    });
}); };
var deleteFromCategory = function (categoryId, productId) { return __awaiter(void 0, void 0, void 0, function () {
    var category, i;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0: return [4 /*yield*/, Category_1.Category.findOne({ _id: categoryId })];
            case 1:
                category = _a.sent();
                if (!category) return [3 /*break*/, 5];
                i = 0;
                _a.label = 2;
            case 2:
                if (!(i < category.products.length)) return [3 /*break*/, 5];
                if (!(category.products[i].toString() === productId.toString())) return [3 /*break*/, 4];
                category.products.splice(i, 1);
                return [4 /*yield*/, category.save()];
            case 3:
                _a.sent();
                return [3 /*break*/, 5];
            case 4:
                i++;
                return [3 /*break*/, 2];
            case 5: return [2 /*return*/];
        }
    });
}); };
exports["default"] = new ProductController;
