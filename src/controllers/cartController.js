"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
exports.__esModule = true;
var User_1 = require("../models/User");
var Cart_1 = require("../models/Cart");
var CartController = /** @class */ (function () {
    function CartController() {
    }
    CartController.prototype.cartCreate = function (req, res) {
        return __awaiter(this, void 0, void 0, function () {
            var cart, user, e_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 5, , 6]);
                        cart = new Cart_1.Cart();
                        return [4 /*yield*/, User_1.User.findOne({ _id: req.user.id })];
                    case 1:
                        user = _a.sent();
                        cart.user = req.user.id;
                        return [4 /*yield*/, cart.save()];
                    case 2:
                        _a.sent();
                        if (!user) return [3 /*break*/, 4];
                        user.cart = cart._id;
                        return [4 /*yield*/, user.save()];
                    case 3:
                        _a.sent();
                        res.json({ cart: cart });
                        _a.label = 4;
                    case 4: return [3 /*break*/, 6];
                    case 5:
                        e_1 = _a.sent();
                        res.status(400).json({ message: "Ошибка создания корзины" });
                        return [3 /*break*/, 6];
                    case 6: return [2 /*return*/];
                }
            });
        });
    };
    CartController.prototype.addProducts = function (req, res) {
        return __awaiter(this, void 0, void 0, function () {
            var user, cart, product, e_2;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 5, , 6]);
                        return [4 /*yield*/, User_1.User.findOne({ _id: req.user.id })];
                    case 1:
                        user = _a.sent();
                        if (!user) return [3 /*break*/, 4];
                        return [4 /*yield*/, Cart_1.Cart.findOne({ _id: user.cart })];
                    case 2:
                        cart = _a.sent();
                        product = req.body.id;
                        if (!cart) return [3 /*break*/, 4];
                        cart.products.push(product);
                        return [4 /*yield*/, cart.save()];
                    case 3:
                        _a.sent();
                        res.json({ message: "\u041F\u0440\u043E\u0434\u0443\u043A\u0442 \u0434\u043E\u0431\u0430\u0432\u043B\u0435\u043D \u0432 \u043A\u043E\u0440\u0437\u0438\u043D\u0443" });
                        _a.label = 4;
                    case 4: return [3 /*break*/, 6];
                    case 5:
                        e_2 = _a.sent();
                        res.status(400).json({ message: "Ошибка добавления в корзину" });
                        return [3 /*break*/, 6];
                    case 6: return [2 /*return*/];
                }
            });
        });
    };
    CartController.prototype.getById = function (req, res) {
        return __awaiter(this, void 0, void 0, function () {
            var cart, e_3;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, Cart_1.Cart.findOne({ _id: req.params.id })];
                    case 1:
                        cart = _a.sent();
                        if (cart) {
                            Cart_1.Cart.findOne({ _id: req.params.id }, function (err, doc) {
                                if (err)
                                    return res.send("error");
                                res.send("\u0418\u043D\u0444\u043E\u0440\u043C\u0430\u0446\u0438\u044F \u043E \u043A\u043E\u0440\u0437\u0438\u043D\u0435 " + doc);
                            });
                        }
                        else
                            res.send("\u0422\u0430\u043A\u043E\u0439 \u043A\u043E\u0440\u0437\u0438\u043D\u044B \u043D\u0435 \u0441\u0443\u0449\u0435\u0441\u0442\u0432\u0443\u0435\u0442");
                        return [3 /*break*/, 3];
                    case 2:
                        e_3 = _a.sent();
                        res.status(400).json({ message: "Get Carts ID error -> " + e_3 });
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    return CartController;
}());
exports["default"] = new CartController;
