"use strict";
exports.__esModule = true;
exports.User = void 0;
var mongoose = require("mongoose");
var model = mongoose.model, Schema = mongoose.Schema;
var userSchema = new Schema({
    email: {
        type: String,
        unique: true,
        required: true
    },
    password: {
        type: String
    },
    name: {
        type: String,
        required: true
    },
    surname: {
        type: String,
        required: true
    },
    role: {
        type: String,
        required: true,
        "default": "USER"
    },
    order: [{
            type: Schema.Types.ObjectId,
            ref: 'Order'
        }],
    cart: {
        type: Schema.Types.ObjectId,
        ref: 'Cart'
    }
});
var User = mongoose.model('User', userSchema);
exports.User = User;
