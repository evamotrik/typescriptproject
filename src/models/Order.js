"use strict";
exports.__esModule = true;
exports.Order = void 0;
var mongoose = require("mongoose");
var model = mongoose.model, Schema = mongoose.Schema;
var orderSchema = new Schema({
    cart: [{
            type: Schema.Types.ObjectId,
            ref: 'Cart'
        }],
    user: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    }
});
var Order = mongoose.model('Category', orderSchema);
exports.Order = Order;
