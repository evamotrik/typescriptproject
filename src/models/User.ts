import * as mongoose from "mongoose"
import {Icart} from "../models/Cart"
import {Iproduct} from "../models/Product"
const {model, Schema} = mongoose

interface Iuser extends mongoose.Document {
   email: string;
   password: string;
   name: string;
   surname: string;
   role: any;
   order: Array<Iproduct>;
   cart: Icart; 
  }

let userSchema = new Schema({
    email:{
        type: String,
        unique: true,
        required: true,
    },
    password:{
        type: String,
    },
    name:{
        type: String,
        required: true,
    },
    surname:{
        type: String,
        required: true,
    },
    role:{
        type: String,
        required: true,
        default: "USER"
    },
    order:[{
        type: Schema.Types.ObjectId,
        ref: 'Order'
    }],
    cart:{
        type: Schema.Types.ObjectId,
        ref: 'Cart',
    }
   
})

const User = mongoose.model<Iuser>('User', userSchema)

export {User, Iuser}
