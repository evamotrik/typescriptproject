"use strict";
exports.__esModule = true;
exports.Category = void 0;
var mongoose = require("mongoose");
var model = mongoose.model, Schema = mongoose.Schema;
var categorySchema = new Schema({
    name: {
        type: String,
        required: true
    },
    products: [{
            type: Schema.Types.ObjectId,
            ref: 'Product'
        }]
});
var Category = mongoose.model('Category', categorySchema);
exports.Category = Category;
