import * as mongoose from "mongoose"
const {model, Schema} =  mongoose

interface Icart extends mongoose.Document {
    products: Array<string>;
    user: string;
   }

const cartSchema = new Schema({
    products: [{
        type: Schema.Types.ObjectId,
        ref: "Product",
    }],
    user: {
        type: Schema.Types.ObjectId,
        ref: 'User',
    },
});

const Cart = mongoose.model<Icart>('Cart', cartSchema)
  
export {Cart, Icart}