"use strict";
exports.__esModule = true;
exports.Cart = void 0;
var mongoose = require("mongoose");
var model = mongoose.model, Schema = mongoose.Schema;
var cartSchema = new Schema({
    products: [{
            type: Schema.Types.ObjectId,
            ref: "Product"
        }],
    user: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    }
});
var Cart = mongoose.model('Cart', cartSchema);
exports.Cart = Cart;
