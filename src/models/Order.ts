import * as mongoose from "mongoose"
const {model, Schema} = mongoose

interface Iorder extends mongoose.Document {
    cart: Array<string>;
    user: string;
   }

const orderSchema = new Schema({
    cart: [{
        type: Schema.Types.ObjectId,
        ref: 'Cart',
    }],
    user: {
        type: Schema.Types.ObjectId,
        ref: 'User',
    }
})

const Order = mongoose.model<Iorder>('Category', orderSchema)

export {Order, Iorder}