import * as mongoose from "mongoose"
const {model, Schema} = mongoose

interface Iproduct extends mongoose.Document {
    name: string;
    price: number;
    category: string;
   }

const productSchema = new Schema({
    name:{
        type: String,
    },
    price:{
        type: Number,
    },
    category:{
        type: Schema.Types.ObjectId,
        ref: 'Category'
    }
});

const Product = mongoose.model<Iproduct>('Product', productSchema)

export {Product, Iproduct}
