"use strict";
exports.__esModule = true;
exports.Product = void 0;
var mongoose = require("mongoose");
var model = mongoose.model, Schema = mongoose.Schema;
var productSchema = new Schema({
    name: {
        type: String
    },
    price: {
        type: Number
    },
    category: {
        type: Schema.Types.ObjectId,
        ref: 'Category'
    }
});
var Product = mongoose.model('Product', productSchema);
exports.Product = Product;
