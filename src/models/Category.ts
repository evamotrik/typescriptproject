import * as mongoose from "mongoose"
const {model, Schema} = mongoose

interface Icategory extends mongoose.Document {
    name: string;
    products: Array<string>;
   }

const categorySchema = new Schema({
    name:{
        type: String,
        required: true,

    },
    products:[{
        type:Schema.Types.ObjectId,
        ref: 'Product'
    }]
})

const Category = mongoose.model<Icategory>('Category', categorySchema)

export {Category, Icategory}