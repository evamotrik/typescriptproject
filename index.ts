import express from 'express'
import allRoutes from './src/routes/allRoutes'
import db from './src/db/db'
import * as bodyParser from 'body-parser'


const PORT = 3000
const app = express()

const parser = express.json()
const urlencodedParser = bodyParser.urlencoded({extended: true})

app.use(parser)
app.use(urlencodedParser)
app.use('/', allRoutes)

app.listen(PORT, ()=>{
    db
    console.log(`Sever is running on port:  ${PORT}`)
})