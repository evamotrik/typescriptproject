"use strict";
exports.__esModule = true;
var express_1 = require("express");
var allRoutes_1 = require("./src/routes/allRoutes");
var db_1 = require("./src/db/db");
var bodyParser = require("body-parser");
var PORT = 3000;
var app = express_1["default"]();
var parser = express_1["default"].json();
var urlencodedParser = bodyParser.urlencoded({ extended: true });
app.use(parser);
app.use(urlencodedParser);
app.use('/', allRoutes_1["default"]);
app.listen(PORT, function () {
    db_1["default"];
    console.log("Sever is running on port:  " + PORT);
});
